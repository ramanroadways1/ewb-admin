<button style="display:none" id="modal_btn" data-toggle="modal" data-target="#ModalUpdateStatus"></button>

<form id="StatusForm" action="#" method="POST">
<div id="ModalUpdateStatus" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update eway remark : <span id="ewb_no_html"></span>
      </div>
      <div class="modal-body">
        <div class="row">
		
		<!--
			<div class="form-group col-md-6">
				<label>Select status <font color="red"><sup>*</sup></font></label>
				<select onchange="DelStatus(this.value)" class="form-control" required="required" name="ewb_status">
					<option value="">--select status--</option>
					<option value="Delivered">Delivered</option>
					<option value="Pending">Delivery Pending</option>
				</select>	
			</div>
			
			<script>
			function DelStatus(elem)
			{
				$('#del_date').val('');
				$('#narration').val('');
				
				if(elem=='Delivered')
				{
					$('#del_date').attr('readonly',false);
					$('#del_date').val($('#ewb_del_date').val());
					$('#narration').attr('readonly',true);
				}
				else if(elem=='Pending')
				{
					$('#del_date').attr('readonly',true);
					$('#narration').attr('readonly',false);
				}
				else
				{
					$('#del_date').attr('readonly',true);
					$('#narration').attr('readonly',true);
				}
			}
			</script>
		-->
		
			<div class="form-group col-md-6">
				<label>Delivery date <font color="red"><sup>*</sup></font></label>
				<input readonly id="del_date" name="del_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
		
		
			<input type="hidden" id="ewb_id" name="ewb_id">
			<input type="hidden" id="ewb_del_date" name="del_date">
			
			<div class="form-group col-md-6">
				<label>Remark/Narration <font color="red"><sup>*</sup></font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,:/]/,'')" id="narration" name="narration" class="form-control" required="required"></textarea>
			</div>
		
		</div>
      </div>
	  <div id="result_Form"></div>
      <div class="modal-footer">
        <button type="submit" id="btn_form" class="btn btn-primary">Submit</button>
        <button type="button" id="modal_close_btn" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#StatusForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#btn_form").attr("disabled", true);
	$.ajax({
        	url: "./save_ewb_remark.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_Form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>