<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);

$get_ewb_data = Qry($conn,"SELECT e.ewb_no,l.lrno,l.company,e.ewb_id,e.truck_no,l.fstation,l.tstation,date(e2.ewb_expiry) as exp_date,
e2.ewb_expiry as ewb_expiry2,e2.fromPincode,e2.toPincode 
FROM _eway_bill_validity AS e 
LEFT OUTER JOIN lr_sample AS l ON l.id = e.lr_id 
LEFT OUTER JOIN _eway_bill_lr_wise AS e2 ON e2.id = e.ewb_id 
WHERE e.id='$id'");

if(!$get_ewb_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row = fetchArray($get_ewb_data);

if($row['exp_date']==0 || $row['exp_date']=='')
{
	// echo "<span style='color:red;font-size:13px'></span>";
	echo "<script>
		alert('Eway-bill does not belongs to our company !')
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($row['exp_date']!=date("Y-m-d"))
{
	// echo "<span style='color:red;font-size:13px'></span>";
	echo "<script>
		alert('Eway-bill expiry date is: $row[exp_date] !')
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$ewb_no = $row['ewb_no'];
$from_loc = $row['fstation'];
$to_loc = $row['tstation'];
$tno = $row['truck_no'];
$lrno = $row['lrno'];
$dest_pincode = $row['toPincode'];

$_SESSION['modal_ewb_ext_dest_pincode'] = $dest_pincode;
$_SESSION['modal_ewb_ext_ewb_no'] = $ewb_no;
$_SESSION['modal_ewb_ext_company'] = $row['company'];
$_SESSION['modal_ewb_ext_ewb_id'] = $row['ewb_id'];
$_SESSION['modal_ewb_ext_prev_exp_date'] = $row['ewb_expiry2'];
?>

<button id="EwbValModalBtn" data-toggle="modal" data-target="#EwbValExtendModal" style="display:none"></button>

<form style="font-size:12px" id="EwbValExtendForm" action="#" method="POST">
<div id="EwbValExtendModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div style="font-size:14px;" class="modal-header bg-primary">
		Extend eway-bill validity : <?php echo $ewb_no; ?>
      </div>
      <div class="modal-body">
        <div class="row">
			
			<div class="form-group col-md-4">
				<label>From Location </label>
				<input style="font-size:12.5px" value="<?php echo $from_loc; ?>" type="text" readonly class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label>To Location <sup style="color:maroon;font-size:11px"><?php echo $dest_pincode; ?></sup></label>
				<input style="font-size:12.5px" value="<?php echo $to_loc; ?>" type="text" readonly class="form-control">
			</div>
			 
			<div class="form-group col-md-4">
				<label>Vehicle number </label>
				<input style="font-size:12.5px" value="<?php echo $tno; ?>" type="text" readonly class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label>Extension reason <font color="red"><sup>*</sup></font></label>
				<select style="font-size:12.5px" name="reason" class="form-control" required="required">
					<option style="font-size:12.5px" value="">--select--</option>
					<option disabled style="font-size:12.5px" value="1">Natural Calamity</option>
					<option disabled style="font-size:12.5px" value="2">Law & Order</option>
					<option disabled style="font-size:12.5px" value="4">Transhipment</option>
					<option disabled style="font-size:12.5px" value="5">Accident</option>
					<option selected style="font-size:12.5px" value="99">Others</option>
				</select>
			</div>
			
			<div class="form-group col-md-4">
				<label>LR number </label>
				<input style="font-size:12.5px" type="text" value="<?php echo $lrno; ?>" readonly class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label>Extension Remark <font color="red"><sup>*</sup></font></label>
				<textarea style="font-size:12.5px" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" name="remark" class="form-control" required="required"></textarea>
			</div>
			
			<div class="form-group bg-primary col-md-12" style="color:#fff;padding:5px;font-size:14px">
				** Current place details ** 
			</div>
			
			<div class="form-group col-md-3">
				<label>Current place <font color="red"><sup>*</sup></font></label>
				<input placeholder="current location" style="font-size:12.5px" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" name="current_place" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3">
				<label>Pincode <font color="red"><sup>*</sup></font></label>
				<input id="modal_ewb_ext_curr_loc_pincode" placeholder="current pincode" maxlength="6" style="font-size:12.5px" type="text" oninput="this.value=this.value.replace(/[^0-9,]/,'')" name="pincode" class="form-control" required="required">
			</div>

<script>		
function CalcDistancePincode()
{
	var pincode = $('#modal_ewb_ext_curr_loc_pincode').val();
		
			$('#ext_val_ewb_btn_save').attr('disabled',true);
			$('#ewb_extd_verify_btn').attr('disabled',true);
			$("#loadicon").show(); 
			jQuery.ajax({
			url: "./fetch_distance_bw_pincode.php",
			data: 'pincode=' + pincode,
			type: "POST",
			success: function(data) {
				$("#result_Ewb_Extend_Form").html(data);
			},
			error: function() {}
			});
}
</script>		
		
			<div class="form-group col-md-3">
				<label>State <font color="red"><sup>*</sup></font></label>
				<select style="font-size:12.5px" id="modal_ewb_ext_state" name="state" class="form-control" required="required">
					<option style="font-size:12.5px" value="">--select--</option>
					<?php
					$qry_get_current_states = Qry($conn,"SELECT name,code FROM state_codes ORDER BY name ASC");
					
					if(numRows($qry_get_current_states)>0)
					{
						while($row_curr_states = fetchArray($qry_get_current_states))
						{
							echo "<option style='font-size:12.5px' value='$row_curr_states[code]'>$row_curr_states[name]</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div class="form-group col-md-3">
				<label>Remaining distance <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px" type="text" name="distance" id="modal_ewb_ext_distance_remaining" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Address line-1 <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" name="addr_1" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Address line-2 <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" name="addr_2" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Address line-3 <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" name="addr_3" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<button type="button" id="ewb_extd_verify_btn" onclick="CalcDistancePincode()" class="btn btn-sm btn-primary">Save details</button>
			</div>
			
			<input type="hidden" value="<?php echo $id; ?>" name="id_modal">
			
			<div class="form-group col-md-8" id="result_Ewb_Extend_Form"></div>
			
		</div>
      </div>
	 
      <div class="modal-footer">
<?php
$fromTime_ewb_ext1 = DateTime::createFromFormat('H:i a', "18:00 pm");
$toTime_ewb_ext1 = DateTime::createFromFormat('H:i a', "23:40 pm");

if(DateTime::createFromFormat('H:i a', date("H:i a")) > $fromTime_ewb_ext1 && DateTime::createFromFormat('H:i a', date("H:i a")) < $toTime_ewb_ext1)		
{
?>
<button type="submit" disabled id="ext_val_ewb_btn_save" class="btn btn-sm btn-primary">Confirm Extension</button>
<?php
}
else
{
	echo "<center><span style='color:red;font-size:13px'>Extension Timing is: 06:00 PM to 11:30 PM !</span></center>";
}
?>
<button type="button" id="ext_modal_hide_btn" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#EwbValExtendForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#ext_val_ewb_btn_save").attr("disabled", true);
	$.ajax({
        	url: "./save_ewb_extend_validity.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_Ewb_Extend_Form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<script>
$('#EwbValModalBtn')[0].click();
$('#loadicon').fadeOut('slow');
</script>