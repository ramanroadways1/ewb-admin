<?php
require_once 'connect.php'; 

$date_tdy = date("Y-m-d");

$sql ="SELECT f.id,f.ewb_no,f.prev_vou,f.current_vou,f.prev_lrno,f.current_lrno,f.crossing,f.branch,f.timestamp,e.name 
FROM _ewb_multiple_fm AS f 
LEFT OUTER JOIN emp_attendance AS e ON e.code = f.branch_user";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'ewb_no', 'dt' => 1),
    array( 'db' => 'prev_vou', 'dt' => 2),
    array( 'db' => 'prev_lrno', 'dt' => 3),
    array( 'db' => 'current_vou', 'dt' => 4),
    array( 'db' => 'current_lrno', 'dt' => 5),
    array( 'db' => 'branch', 'dt' => 6),
    array( 'db' => 'name', 'dt' => 7), 
    array( 'db' => 'crossing', 'dt' => 8), 
    array( 'db' => 'timestamp', 'dt' => 9), 
  );
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../b5aY6EZzK52NA8F/scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);