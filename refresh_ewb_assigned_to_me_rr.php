<?php
include($_SERVER['DOCUMENT_ROOT']."/_connect.php");
// require($_SERVER['DOCUMENT_ROOT']."/graph/smtp/PHPMailerAutoload.php");
$timestamp = date("Y-m-d H:i:s");
$date_ewb = date('d-m-Y', strtotime('-1 days'));
$company = "RAMAN_ROADWAYS";
$branch = "ADMIN";
$branch_user = "AutoScript";

$get_token = Qry($conn,"SELECT token FROM ship.api_token WHERE company='$company' ORDER BY id DESC LIMIT 1");
		
if(!$get_token){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_token = fetchArray($get_token);		

$stateCode="01";		
$ewb_token = $row_token['token'];

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "$tax_pro_url/v1.03/dec/ewayapi?action=GetEwayBillsForTransporterByState&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$rr_gst_no&username=$ewb_rr_username&authtoken=$ewb_token&date=$date_ewb&stateCode=$stateCode",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response_main = curl_exec($curl);
curl_close($curl);

// echo $response_main;
// exit();

$response = json_decode($response_main, true);
		
if(@$response['error']) // if error
{
	$error_msg = $response['error']['message'];
			
	if($response['error']['error_cd']=="GSP102")
	{
		$token_func = GenerateEwbToken($conn,$company);
		
		if($token_func[0]=="ERROR")
		{
			echo $token_func[1];
			exit();
		}
		
		$ewb_token = $token_func[1];
		
		$get_states = Qry($conn,"SELECT code FROM state_codes");
	
		while($row_state = fetchArray($get_states))
		{
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "$tax_pro_url/v1.03/dec/ewayapi?action=GetEwayBillsForTransporterByState&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$rr_gst_no&username=$ewb_rr_username&authtoken=$ewb_token&date=$date_ewb&stateCode=$row_state[code]",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'GET',
			));

			$response_main = curl_exec($curl);
			curl_close($curl);

			$response = json_decode($response_main, true);
			
			if(empty($response))
			{
				
			}
			else
			{
				foreach($response as $data)
				{
					$ewbNo = @$data['ewbNo'];
					$ewbDate = date("Y-m-d H:i:s",strtotime(str_replace('/','-',@$data['ewbDate'])));
					$status = @$data['status'];
					$genGstin = @$data['genGstin'];
					$docNo = @$data['docNo'];
					$docDate = date("Y-m-d",strtotime(str_replace('/','-',@$data['docDate'])));
					$delPinCode = @$data['delPinCode'];
					$delStateCode = @$data['delStateCode'];
					$delPlace = @$data['delPlace'];
					$validUpto = date("Y-m-d H:i:s",strtotime(str_replace('/','-',@$data['validUpto'])));
					$extendedTimes = @$data['extendedTimes'];
					$rejectStatus = @$data['rejectStatus'];
					
					if(!empty($ewbNo))
					{
						$chk_record = Qry($conn,"SELECT id FROM _ewb_server_temp WHERE date(timestamp)='".date("Y-m-d")."' AND ewbNo='$ewbNo'");
						
						if(numRows($chk_record)==0)
						{
							$insert_ewb = Qry($conn,"INSERT INTO _ewb_server_temp(json_res,company,ewbNo,ewbDate,status,genGstin,docNo,docDate,delPinCode,delStateCode,
							delPlace,validUpto,extendedTimes,rejectStatus,timestamp) VALUES ('$response_main','$company','$ewbNo','$ewbDate','$status','$genGstin','$docNo',
							'$docDate','$delPinCode','$delStateCode','$delPlace','$validUpto','$extendedTimes','$rejectStatus','$timestamp')");

							if(!$insert_ewb){
								errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
								exit();
							}
						}
					}
				}
			}
		}
	}
	else
	{
		// echo $response_main."<br>".$error_msg;
			// exit();
		if(empty($response))
		{
			
		}
		else
		{
			$get_states = Qry($conn,"SELECT code FROM state_codes");
	
			while($row_state = fetchArray($get_states))
			{
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_URL => "$tax_pro_url/v1.03/dec/ewayapi?action=GetEwayBillsForTransporterByState&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$rr_gst_no&username=$ewb_rr_username&authtoken=$ewb_token&date=$date_ewb&stateCode=$row_state[code]",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'GET',
				));

				$response_main = curl_exec($curl);
				curl_close($curl);

				$response = json_decode($response_main, true);
				
				if(empty($response))
				{
					
				}
				else
				{
					foreach($response as $data)
					{
						$ewbNo = @$data['ewbNo'];
						$ewbDate = date("Y-m-d H:i:s",strtotime(str_replace('/','-',@$data['ewbDate'])));
						$status = @$data['status'];
						$genGstin = @$data['genGstin'];
						$docNo = @$data['docNo'];
						$docDate = date("Y-m-d",strtotime(str_replace('/','-',@$data['docDate'])));
						$delPinCode = @$data['delPinCode'];
						$delStateCode = @$data['delStateCode'];
						$delPlace = @$data['delPlace'];
						$validUpto = date("Y-m-d H:i:s",strtotime(str_replace('/','-',@$data['validUpto'])));
						$extendedTimes = @$data['extendedTimes'];
						$rejectStatus = @$data['rejectStatus'];
						
						if(!empty($ewbNo))
						{
							$chk_record = Qry($conn,"SELECT id FROM _ewb_server_temp WHERE date(timestamp)='".date("Y-m-d")."' AND ewbNo='$ewbNo'");
						
							if(numRows($chk_record)==0)
							{
								$insert_ewb = Qry($conn,"INSERT INTO _ewb_server_temp(json_res,company,ewbNo,ewbDate,status,genGstin,docNo,docDate,delPinCode,delStateCode,
								delPlace,validUpto,extendedTimes,rejectStatus,timestamp) VALUES ('$response_main','$company','$ewbNo','$ewbDate','$status','$genGstin','$docNo',
								'$docDate','$delPinCode','$delStateCode','$delPlace','$validUpto','$extendedTimes','$rejectStatus','$timestamp')");

								if(!$insert_ewb){
									errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
									exit();
								}
							}
						}
					}
				}
			}
		}
	}
}
else
{
	$get_states = Qry($conn,"SELECT code FROM state_codes WHERE code!='$stateCode'");
	
	while($row_state = fetchArray($get_states))
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "$tax_pro_url/v1.03/dec/ewayapi?action=GetEwayBillsForTransporterByState&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$rr_gst_no&username=$ewb_rr_username&authtoken=$ewb_token&date=$date_ewb&stateCode=$row_state[code]",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		));

		$response_main = curl_exec($curl);
		curl_close($curl);

		$response = json_decode($response_main, true);
		
		if(empty($response))
		{
			
		}
		else
		{
			foreach($response as $data)
			{
				$ewbNo = @$data['ewbNo'];
				$ewbDate = date("Y-m-d H:i:s",strtotime(str_replace('/','-',@$data['ewbDate'])));
				$status = @$data['status'];
				$genGstin = @$data['genGstin'];
				$docNo = @$data['docNo'];
				$docDate = date("Y-m-d",strtotime(str_replace('/','-',@$data['docDate'])));
				$delPinCode = @$data['delPinCode'];
				$delStateCode = @$data['delStateCode'];
				$delPlace = @$data['delPlace'];
				$validUpto = date("Y-m-d H:i:s",strtotime(str_replace('/','-',@$data['validUpto'])));
				$extendedTimes = @$data['extendedTimes'];
				$rejectStatus = @$data['rejectStatus'];
				
				if(!empty($ewbNo))
				{
					$chk_record = Qry($conn,"SELECT id FROM _ewb_server_temp WHERE date(timestamp)='".date("Y-m-d")."' AND ewbNo='$ewbNo'");
					
					if(numRows($chk_record)==0)
					{
						$insert_ewb = Qry($conn,"INSERT INTO _ewb_server_temp(json_res,company,ewbNo,ewbDate,status,genGstin,docNo,docDate,delPinCode,delStateCode,
						delPlace,validUpto,extendedTimes,rejectStatus,timestamp) VALUES ('$response_main','$company','$ewbNo','$ewbDate','$status','$genGstin','$docNo',
						'$docDate','$delPinCode','$delStateCode','$delPlace','$validUpto','$extendedTimes','$rejectStatus','$timestamp')");

						if(!$insert_ewb){
							errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
							exit();
						}
					}
				}
			}
		}
	}
}

// $copy_data = Qry($conn,"SELECT e.del_status,e.smsstatus,s.sender_id,s.status_code,s.status,s.mobile,s.timestamp 
// FROM _webhook_pinnacle_sms_error AS e 
// LEFT JOIN _webhook_pinnacle_sms AS s ON s.id=e.webhook_id 
// WHERE e.mail_sent!='1'");

// if(!$copy_data){
	// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	// exit();
// }
?>
<script>
	FetchData();
</script>