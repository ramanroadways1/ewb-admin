<?php
session_start();

include($_SERVER['DOCUMENT_ROOT']."/_connect.php"); // for localhost

date_default_timezone_set('Asia/Kolkata');
$timestamp=date("Y-m-d H:i:s");

$page_name=$_SERVER['REQUEST_URI'];
$page_url=$_SERVER['REQUEST_URI'];
$this_page_url = basename($_SERVER['PHP_SELF']);

if(!isset($_SESSION['ewb_admin']))
{
	echo "<script>
		window.location.href='./logout.php';
	</script>";
	exit();
}
?>