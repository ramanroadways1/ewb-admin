<?php
require_once 'connect.php'; 

$date_tdy = date("Y-m-d");

$sql ="SELECT c.id,c.ewb_no,DATE_FORMAT(c.ewb_date,'%d-%m-%y') as ewb_date,c.timestamp,e.lrno,e.branch,e.truck_no,
DATE_FORMAT(e.lr_date,'%d-%m-%y') as lr_date,CONCAT(e.from_loc,'<br>',e.to_loc) as location,CONCAT(e.consignor,'<br>',e.consignee) as party 
FROM _ewb_cancelled AS c 
LEFT OUTER JOIN _eway_bill_validity AS e ON e.ewb_id = c.ewb_id 
WHERE c.is_checked!='1' ORDER BY c.id ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'ewb_no', 'dt' => 1),
    array( 'db' => 'branch', 'dt' => 2),
    array( 'db' => 'lrno', 'dt' => 3),
    array( 'db' => 'truck_no', 'dt' => 4),
    array( 'db' => 'ewb_date', 'dt' => 5), 
    array( 'db' => 'lr_date', 'dt' => 6), 
    array( 'db' => 'location', 'dt' => 7),  
    array( 'db' => 'party', 'dt' => 8), 
    array( 'db' => 'timestamp', 'dt' => 9), 
 );
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../b5aY6EZzK52NA8F/scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);