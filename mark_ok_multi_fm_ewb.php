<?php
require_once 'connect.php';
 
$id = escapeString($conn,($_POST['id']));
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$insert = Qry($conn,"INSERT INTO _ewb_multiple_fm_db(ewb_no,prev_lrno,prev_vou,current_lrno,prev_vou_date,current_vou,current_vou_date,crossing,
branch,branch_user,timestamp,mark_ok_timestamp) SELECT ewb_no,prev_lrno,prev_vou,current_lrno,prev_vou_date,current_vou,current_vou_date,
crossing,branch,branch_user,timestamp,'$timestamp' FROM _ewb_multiple_fm WHERE id='$id'");

if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$delete = Qry($conn,"DELETE FROM _ewb_multiple_fm WHERE id='$id'");

if(!$delete){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	echo "<script>
		$('#btn_$id').attr('disabled',true);
		$('#btn_$id').attr('onclick','');
	</script>";
	exit();
?>
