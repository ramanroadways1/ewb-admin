<?php
require_once 'connect.php'; 

$date_tdy = date("Y-m-d");

$sql ="SELECT e.id,e.vou_no,e.lrno,e.branch,e.ewb_no,e.truck_no,DATE_FORMAT(e.ewb_date,'%d-%m-%y') as ewb_date,
DATE_FORMAT(e.lr_date,'%d-%m-%y') as lr_date,
DATE_FORMAT(e.ewb_expiry,'%d-%m-%y') as ewb_exp_date,CONCAT(e.from_loc,'<br>',e.to_loc) as location,e.ewb_flag_desc,
CONCAT(e.consignor,'<br>',e.consignee) as party,e.del_date,emp.name as user_name,
IF(e.del_date=0,branch_narration,CONCAT('Delivered- ',DATE_FORMAT(e.del_date,'%d-%m-%y'))) as branch_narration,
DATE_FORMAT(e.branch_timestamp,'%d-%m-%y %H:%i') as updated_at,IF(e.crossing_lr='0','NO','YES') as crossing_lr,e.cross_veh_no,e.ext_remark 
FROM _eway_bill_validity AS e 
LEFT JOIN emp_attendance AS emp ON emp.code=e.update_by_user 
WHERE e.ho_check='0' AND e.branch_timestamp IS NOT NULL ORDER BY e.id ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'branch', 'dt' => 1),
    array( 'db' => 'vou_no', 'dt' => 2),
    array( 'db' => 'lrno', 'dt' => 3),
    array( 'db' => 'truck_no', 'dt' => 4),
    array( 'db' => 'crossing_lr', 'dt' => 5), 
    array( 'db' => 'cross_veh_no', 'dt' => 6), 
    array( 'db' => 'lr_date', 'dt' => 7), 
    array( 'db' => 'ewb_date', 'dt' => 8), 
    array( 'db' => 'ewb_exp_date', 'dt' => 9), 
    array( 'db' => 'location', 'dt' => 10),  
    array( 'db' => 'party', 'dt' => 11), 
    array( 'db' => 'ewb_no', 'dt' => 12), 
    array( 'db' => 'branch_narration', 'dt' => 13), 
	 array(
        'db'        => 'user_name',
        'dt'        => 14,
        'formatter' => function( $d, $row ) {
            return htmlspecialchars($d);
        }
    ),
    // array( 'db' => 'user_name', 'dt' => 13), 
    array( 'db' => 'updated_at', 'dt' => 15), 
    array( 'db' => 'ext_remark', 'dt' => 16), 
    array( 'db' => 'del_date', 'dt' => 17), 
    array( 'db' => 'ewb_flag_desc', 'dt' => 18), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../b5aY6EZzK52NA8F/scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);