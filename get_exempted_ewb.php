<?php
require_once 'connect.php'; 

$date_tdy = date("Y-m-d");

$sql ="SELECT e.id,e.vou_no,e.lrno,e.branch,e.truck_no,DATE_FORMAT(e.lr_date,'%d-%m-%y') as lr_date,e.ewb_flag_desc,
CONCAT(e.from_loc,'<br>',e.to_loc) as location,
CONCAT(e.consignor,'<br>',e.consignee) as party,IF(e.crossing_lr='0','NO','YES') as crossing_lr,e.cross_veh_no
FROM _eway_bill_validity AS e 
WHERE e.ho_check=0 AND date(e.timestamp)='$date_tdy' AND e.ewb_expiry=0 AND 
e.ewb_flag_desc IN('Admin Exempt','Consignor Exempt','Invoice Value Exempt','Salt Exempt','Wolkem Export Exempt') ORDER BY e.id ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'branch', 'dt' => 1),
    array( 'db' => 'vou_no', 'dt' => 2),
    array( 'db' => 'lrno', 'dt' => 3),
    array( 'db' => 'truck_no', 'dt' => 4),
    array( 'db' => 'crossing_lr', 'dt' => 5), 
    array( 'db' => 'cross_veh_no', 'dt' => 6), 
    array( 'db' => 'lr_date', 'dt' => 7), 
    array( 'db' => 'location', 'dt' => 8),  
    array( 'db' => 'party', 'dt' => 9), 
    array( 'db' => 'ewb_flag_desc', 'dt' => 10), 
 );
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../b5aY6EZzK52NA8F/scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);