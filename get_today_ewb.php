<?php
require_once 'connect.php'; 

$date_tdy = date("Y-m-d");

$sql ="SELECT f.id,f.company,f.ewbNo,f.ewbDate,f.genGstin,f.docNo,f.docDate,f.delPincode,f.delPlace,f.ValidUpto,l.lrno 
FROM _ewb_server_temp AS f 
LEFT OUTER JOIN _eway_bill_validity AS l ON l.ewb_no = f.ewbNo";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'ewbNo', 'dt' => 1),
    array( 'db' => 'lrno', 'dt' => 2),
    array( 'db' => 'company', 'dt' => 3),
    array( 'db' => 'ewbDate', 'dt' => 4),
    array( 'db' => 'ValidUpto', 'dt' => 5),
    array( 'db' => 'genGstin', 'dt' => 6), 
    array( 'db' => 'docNo', 'dt' => 7), 
    array( 'db' => 'docDate', 'dt' => 8),  
    array( 'db' => 'delPlace', 'dt' => 9), 
    array( 'db' => 'delPincode', 'dt' => 10), 
 );
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../b5aY6EZzK52NA8F/scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);