<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
?>
<html>

<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
<link rel="icon" type="image/png" href="../b5aY6EZzK52NA8F/favicon.png" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link rel="stylesheet" href="../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../b5aY6EZzK52NA8F/data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
.ui-autocomplete { z-index:2147483647; } 

.modal-backdrop{
   backdrop-filter: blur(5px);
   background-color: #01223770;
}
.modal-backdrop.in{
   opacity: 1 !important;
}

::-webkit-scrollbar{
    width: 6px;
	height:6px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
</style>

<style>
label{
	font-size:13px;
	text-transform:none;
}
@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}
</style>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:150px" src="https://rrpl.online/diary/load.gif" /></center>
</div>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<a href="./"><button class="pull-left btn btn-sm btn-default"><span class="fa fa-check-circle-o"></span> Pending check</button></a>
			<a href="./pending_status.php"><button style="margin-left:10px" class="pull-left btn btn-sm btn-default"><span class="fa fa-exclamation-triangle"></span> Pending status</button></a>
		</div>
		<div class="col-md-4">
			<center><h5 style="">Eway-bill Report :</span></h5>
		</div>
		<div class="col-md-4">
			<a href="./logout.php"><button class="pull-right btn btn-sm btn-default"><span class="glyphicon glyphicon-log-out"></span> Logout</button></a>
		</div>
	</div>	
</div>

<div class="form-group col-md-2">
	<label>From Date <sup><font color="red">*</font></sup></label>
	<input style="font-size:12px" id="from_date" type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
</div>

<div class="form-group col-md-2">
	<label>To Date <sup><font color="red">*</font></sup></label>
	<input style="font-size:12px" id="to_date" type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
</div>

<div class="form-group col-md-2">
	<label>Record Type <sup><font color="red">*</font></sup></label>
	<select style="font-size:12px" id="record_type" type="record_type" class="form-control" required>
		<option style="font-size:12px" value="">--select--</option>
		<option style="font-size:12px" value="ALL">ALL</option>
		<option style="font-size:12px" value="Success">Success with EWB</option>
		<option style="font-size:12px" value="Admin Exempt">Admin Exempt</option>
		<option style="font-size:12px" value="Consignor Exempt">Consignor Exempt</option>
		<option style="font-size:12px" value="Invoice Value Exempt">Invoice Value Exempt</option>
		<option style="font-size:12px" value="Salt Exempt">Salt Exempt</option>
		<option style="font-size:12px" value="Wolkem Export Exempt">Wolkem Export Exempt</option>
	</select>
</div>

<div class="form-group col-md-2">
	<label>&nbsp;</label>
	<br />
	<button type="button" id="report_btn" onclick="FetchData2();" class="btn btn-sm btn-primary">Get records</button>
</div>

	<div class="form-group col-md-12" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>Id</th>
					<th>Branch</th>
					<th>LR_No</th>
					<th>Vehicle_No</th>
					<th>Crossing LR</th>
					<th>Crossing Veh.No</th>
					<th>LR_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Ewb_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Ewb_Expiry_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Location</th>
					<th>Consignor <span class="glyphicon glyphicon-filter"></span></th>
					<th>Ewb_No</th>
					<th>Branch_Status</th>
					<th>Updated_By</th>
					<th>Updated_At</th>
					<th>#</th>
					<th>#Status#</th>
					<th>#</th>
				</tr>
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<div id="edjsakld"></div>

<script type="text/javascript">
function FetchData(fromdate,todate,record_type){
// $("#loadicon").show();
var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		"excel","colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../b5aY6EZzK52NA8F/load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	{ 
    "targets": 10, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	{ 
    "targets": 4, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	{ 
    "targets": 15, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	{
		"visible": false,
    "searchable": false ,
        targets: 16, // edit column order
        data: null,
        mRender: function(data, type, full){
         // return '<input type="hidden" value="'+full[15]+'" id="del_date_ip_'+full[0]+'"><button type="button" class="btn-xs btn-success" onclick="UpdateDelivered('+full[0]+')" id="status_del_btn_'+full[0]+'"><i class="fa fa-check-circle-o" aria-hidden="true"></i> OK</button> or <button type="button" class="btn-xs btn-primary" onclick="UpdateStatus('+full[0]+','+full[2]+','+full[11]+')" id="status_btn_'+full[0]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Remark</button>';
         return '<input type="hidden" value="'+full[15]+'" id="del_date_ip_'+full[0]+'"><button type="button" class="btn-xs btn-success" onclick="UpdateDelivered('+full[0]+')" id="status_del_btn_'+full[0]+'"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Mark Ok</button>';
        }
      },
	  {
		  "visible": false,
    "searchable": false ,
        targets: 17, // edit column order
        data: null,
        mRender: function(data, type, full){
         return '<button type="button" class="btn-xs btn-primary" onclick="MoveToPending('+full[0]+')" id="move_btn_'+full[0]+'"><i class="fa fa-refresh" aria-hidden="true"></i> Mark Pending</button>';
        }
      },
	], 
        "serverSide": true,
		"ajax": {
		"url": "get_ewb_report.php",
		"type": "POST",
		"data": {
			"from_date": fromdate,"to_date": todate,"record_type": record_type
		}},
        "initComplete": function( settings, json ) {
 		// $("#loadicon").hide();
		$('#report_btn').attr('disabled',false);
		$('#report_btn').html('Get records');
 		},
		"bDestroy": true
    } );
}
</script>

<script>
FetchData('<?php echo date("Y-m-d"); ?>','<?php echo date("Y-m-d"); ?>','Success');

function FetchData2()
{
	$('#report_btn').attr('disabled',true);
	$('#report_btn').html('Please wait..');
	
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val();
	var record_type = $('#record_type').val();
	
	if(from_date!='' && to_date!='' && record_type!='')
	{
		FetchData(from_date,to_date,record_type);
	}
	else
	{
		alert('Please enter dates and select record type first !');
		$('#report_btn').attr('disabled',false);
		$('#report_btn').html('Get records');
	}
}
</script>

<div id="function_result"></div>