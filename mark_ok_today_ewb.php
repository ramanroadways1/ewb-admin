<?php
require_once 'connect.php';
 
$id = escapeString($conn,($_POST['id']));
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$insert = Qry($conn,"INSERT INTO _ewb_server(company,ewbNo,ewbDate,status,genGstin,docNo,docDate,delPinCode,delStateCode,delPlace,validUpto,
extendedTimes,rejectStatus,timestamp,check_timestamp) SELECT company,ewbNo,ewbDate,status,genGstin,docNo,docDate,delPinCode,delStateCode,delPlace,validUpto,
extendedTimes,rejectStatus,timestamp,'$timestamp' FROM _ewb_server_temp WHERE id='$id'");

if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$delete = Qry($conn,"DELETE FROM _ewb_server_temp WHERE id='$id'");

if(!$delete){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	echo "<script>
		$('#btn_$id').attr('disabled',true);
		$('#btn_$id').attr('onclick','');
	</script>";
	exit();
?>
