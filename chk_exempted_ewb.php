<?php
require_once 'connect.php';

$id=escapeString($conn,($_POST['ewb_id']));
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$update = Qry($conn,"UPDATE _eway_bill_validity SET ho_check='1',ho_narration='Checked',ho_check_timestamp='$timestamp' WHERE id='$id'");

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	echo "<script>
		alert('Ok : Status updated !!');
		$('#btn_$id').attr('disabled',true);
		$('#btn_$id').html('Updated');
		$('#loadicon').hide();
	</script>";
	exit();
?>
