<?php
require_once 'connect.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));

$sql = Qry($conn,"SELECT date(e.ewb_expiry) as exp_date,l.company,e.ewb_no FROM _eway_bill_validity AS e 
LEFT JOIN lr_sample AS l ON l.id=e.lr_id 
WHERE e.id='$id'");

if(!$sql){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error.","./");
	exit();
}

if(numRows($sql)==0)
{
	echo "<script>alert('Record not found..');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row = fetchArray($sql);

$company = $row['company'];
$ewbNo = $row['ewb_no'];

if(strlen($ewbNo)!=12)
{
	echo "<script>alert('Check ewb number.');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(strtotime($row['exp_date'])>strtotime($date))
{
	echo "<script>alert('Eway bill not expired yet.');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_token=Qry($conn,"SELECT token FROM ship.api_token WHERE company='$company' ORDER BY id DESC LIMIT 1");
if(!$get_token){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$branch="EWAY_BILL";

$row_token=fetchArray($get_token);
$authToken=$row_token['token'];

if($company=='RRPL')
{
	$gst_no_company=$rrpl_gst_no;
	$gst_username=$ewb_rrpl_username;
	$gst_password=$ewb_rrpl_password;
}
else
{
	$gst_no_company=$rr_gst_no;
	$gst_username=$ewb_rr_username;
	$gst_password=$ewb_rr_password;
}

$url_curl="$tax_pro_url/v1.03/dec/ewayapi?action=GetEwayBill&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$gst_no_company&username=$gst_username&authtoken=$authToken&ewbNo=$ewbNo";

	$Curl_EwbFetch = curl_init();
	curl_setopt_array($Curl_EwbFetch, array(
	  CURLOPT_URL => $url_curl,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	));
	
	$response_gen = curl_exec($Curl_EwbFetch);
		$err = curl_error($Curl_EwbFetch);
		curl_close($Curl_EwbFetch);
		
		if($err)
		{
			$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,line_no,error_desc,lrno,branch,timestamp) VALUES 
			('$company','89','cURL Error : $err','RRPL.ONLINE','$branch','$timestamp')");
			if(!$insert_error){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
			}
				
			echo "<script>
				alert('Error: While Fetching Eway-Bill. 105');
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		$response_decoded = json_decode($response_gen, true);
		
		if(@$response_decoded['error'])
		{
			if($response_decoded['error']['error_cd']=="GSP102")
			{
				$curlToken = curl_init();
				curl_setopt_array($curlToken, array(
				  CURLOPT_URL => "$tax_pro_url/v1.03/dec/authenticate?action=ACCESSTOKEN&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$gst_no_company&username=$gst_username&ewbpwd=$gst_password",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'GET',
				));
				
				$response_Token = curl_exec($curlToken);
				$errToken = curl_error($curlToken);
				curl_close($curlToken);
				
				if($errToken)
				{
					$insert_error2 = Qry($conn,"INSERT INTO ship.eway_bill_error(company,line_no,error_desc,lrno,branch,timestamp) VALUES 
					('$company','136','cURL Error : $errToken','RRPL.ONLINE','$branch','$timestamp')");
					if(!$insert_error2){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
							Redirect("Error while processing Request","../");
							exit();
					}
						
					echo "<script>
						alert('Error: While Fetching Eway-Bill Token.');
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$response_decodedToken1 = json_decode($response_Token, true);
				$authToken_new = $response_decodedToken1['authtoken'];
				
				if($authToken_new=='')
				{
					echo "<script>
						alert('Error : Empty token. Please try once again..');
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$insert_token=Qry($conn,"INSERT INTO ship.api_token (token,company,user,branch_user,timestamp) VALUES 
				('$authToken_new','$company','$branch','$branch_sub_user','$timestamp')");
				if(!$insert_token){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
				}
				
				echo "<script>
						alert('Please try again !!');
						$('#loadicon').hide();
					</script>";
				exit();
				
			}
			else if($response_decoded['error']['error_cd']=="325")
			{
				echo "<script>
						alert('Error: Invalid Eway-Bill Number : $ewbNo.');
						$('#loadicon').hide();
					</script>";
				exit();
			}
			else
			{
				// echo $response_gen."<br>";
				$errorMsg = $response_decoded['error']['message'];
				$errorMsgCode = $response_decoded['error']['error_cd'];
				
				$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,line_no,error_desc,lrno,msg,branch,timestamp) VALUES 
				('$company','192','$response_gen : $url_curl','RRPL.ONLINE','$errorMsg','$branch','$timestamp')");
				
				if(!$insert_error){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
				}
				
				echo "<script>
						alert('Error: While Fetching Eway-Bill. Code : $errorMsgCode.');
						$('#loadicon').hide();
					</script>";
				exit();
			}
		}
	
	$i_veh_no = 0;
	
	if(empty($response_decoded['VehiclListDetails']))
	{
		$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,line_no,error_desc,lrno,msg,branch,timestamp) VALUES 
		('$company','221','$response_gen : $url_curl','RRPL.ONLINE','NOT-FOUND','$branch','$timestamp')");
				
				if(!$insert_error){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
				}
				
		echo "<script>
				alert('Error: While Fetching Eway-Bill. VL');
				$('#lr_sub').attr('disabled', true);
				$('#eway_bill_no').val('');
				$('#loadicon').hide();
				// window.close();
			</script>";
		exit();
	}	
	
	foreach($response_decoded['VehiclListDetails'] as $Data1){
		if($i_veh_no == 0) {
			$Veh_no=$Data1['vehicleNo'];
		}
	$i_veh_no++;
		// $Veh_no=$Data1['vehicleNo'];
	}
		
	$validUpto = $response_decoded['validUpto'];		
	$extendedTimes = $response_decoded['extendedTimes'];	
	$validUpto = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$validUpto)));
	$billDate2 = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$response_decoded['ewayBillDate'])));
	
	// echo "<script>
		// alert('OK : $validUpto !');
		// window.location.href='./pending_status.php';
	// </script>";
	// exit();
	
$update_new_update=Qry($conn,"UPDATE _eway_bill_validity SET ewb_expiry='$validUpto',ewb_extended='$extendedTimes',last_get='$timestamp' 
WHERE ewb_no='$ewbNo'");

if(!$update_new_update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

$update_new_update2=Qry($conn,"UPDATE _eway_bill_lr_wise SET ewb_expiry='$validUpto',ewb_extended='$extendedTimes' WHERE ewb_no='$ewbNo'");

if(!$update_new_update2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}
		
if(isset($_POST['page']))
{
	echo "<script>
		alert('OK : Fetched successfully !');
		window.location.href='./';
	</script>";
	exit();
}	
else
{
	echo "<script>
		alert('OK : Fetched successfully !');
		window.location.href='./pending_status.php';
	</script>";
	exit();
}
	
?>