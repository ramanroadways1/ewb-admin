<?php
require_once 'connect.php'; 

$date_tdy = date("Y-m-d");

$from_date = escapeString($conn,$_POST['from_date']);
$to_date = escapeString($conn,$_POST['to_date']);
$record_type = escapeString($conn,$_POST['record_type']);

if($record_type=='ALL')
{
	$sql ="SELECT e.id,e.lrno,e.branch,IF(e.ewb_no='',e.ewb_flag_desc,e.ewb_no) as ewb_no,e.truck_no,
	DATE_FORMAT(e.ewb_date,'%d-%m-%y') as ewb_date,
	DATE_FORMAT(e.lr_date,'%d-%m-%y') as lr_date,
	DATE_FORMAT(e.ewb_expiry,'%d-%m-%y') as ewb_exp_date,CONCAT(e.from_loc,'<br>',e.to_loc) as location,
	CONCAT(e.consignor,'<br>',e.consignee) as party,e.del_date,emp.name as user_name,
	IF(e.del_date=0,branch_narration,CONCAT('Delivered- ',DATE_FORMAT(e.del_date,'%d-%m-%y'))) as branch_narration,
	DATE_FORMAT(e.branch_timestamp,'%d-%m-%y %H:%i') as updated_at,IF(e.crossing_lr='0','NO','YES') as crossing_lr,e.cross_veh_no 
	FROM _eway_bill_validity AS e 
	LEFT JOIN emp_attendance AS emp ON emp.code=e.update_by_user 
	WHERE e.lr_date BETWEEN '$from_date' AND '$to_date' ORDER BY e.id ASC";
}
else
{
	$sql ="SELECT e.id,e.lrno,e.branch,IF(e.ewb_no='',e.ewb_flag_desc,e.ewb_no) as ewb_no,e.truck_no,
	DATE_FORMAT(e.ewb_date,'%d-%m-%y') as ewb_date,
	DATE_FORMAT(e.lr_date,'%d-%m-%y') as lr_date,
	DATE_FORMAT(e.ewb_expiry,'%d-%m-%y') as ewb_exp_date,CONCAT(e.from_loc,'<br>',e.to_loc) as location,
	CONCAT(e.consignor,'<br>',e.consignee) as party,e.del_date,emp.name as user_name,
	IF(e.del_date=0,branch_narration,CONCAT('Delivered- ',DATE_FORMAT(e.del_date,'%d-%m-%y'))) as branch_narration,
	DATE_FORMAT(e.branch_timestamp,'%d-%m-%y %H:%i') as updated_at,IF(e.crossing_lr='0','NO','YES') as crossing_lr,e.cross_veh_no 
	FROM _eway_bill_validity AS e 
	LEFT JOIN emp_attendance AS emp ON emp.code=e.update_by_user 
	WHERE e.lr_date BETWEEN '$from_date' AND '$to_date' AND e.ewb_flag_desc='$record_type' ORDER BY e.id ASC";
}

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'branch', 'dt' => 1),
    array( 'db' => 'lrno', 'dt' => 2),
    array( 'db' => 'truck_no', 'dt' => 3),
    array( 'db' => 'crossing_lr', 'dt' => 4), 
    array( 'db' => 'cross_veh_no', 'dt' => 5), 
    array( 'db' => 'lr_date', 'dt' => 6), 
    array( 'db' => 'ewb_date', 'dt' => 7), 
    array( 'db' => 'ewb_exp_date', 'dt' => 8), 
    array( 'db' => 'location', 'dt' => 9),  
    array( 'db' => 'party', 'dt' => 10), 
    array( 'db' => 'ewb_no', 'dt' => 11), 
    array( 'db' => 'branch_narration', 'dt' => 12), 
	 array(
        'db'        => 'user_name',
        'dt'        => 13,
        'formatter' => function( $d, $row ) {
            return htmlspecialchars($d);
        }
    ),
    // array( 'db' => 'user_name', 'dt' => 13), 
    array( 'db' => 'updated_at', 'dt' => 14), 
    array( 'db' => 'del_date', 'dt' => 15), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../b5aY6EZzK52NA8F/scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);